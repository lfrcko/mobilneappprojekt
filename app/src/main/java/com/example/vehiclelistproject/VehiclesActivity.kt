package com.example.vehiclelistproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import com.example.vehiclelistproject.databinding.ActivityVehiclesBinding
import com.example.vehiclelistproject.entities.Vehicle
import kotlinx.coroutines.launch

class VehiclesActivity : MainActivity() {
    private lateinit var binding: ActivityVehiclesBinding
    private lateinit var vehicleList: ArrayList<Vehicle>
    private lateinit var vehicleNamelist: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityVehiclesBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.floatButton.setOnClickListener {
            val intent = Intent(this, VehicleFormActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loadVehicles() {
        vehicleList = ArrayList()
        vehicleNamelist = ArrayList()
        lifecycleScope.launch {
            for (vehicle in db.vehicleDao().getVehicles()) {
                vehicleList.add(vehicle)
                vehicleNamelist.add(vehicle.vehicleName + ", " + vehicle.kilometers + ": " + db.userDao().getUserById(vehicle.userId).userName)
            }

            val adapter: ArrayAdapter<String?> = ArrayAdapter<String?>(
                this@VehiclesActivity,
                R.layout.list_items,
                vehicleNamelist as List<String?>
            )

            binding.lvVehicles.adapter = adapter
        }
    }

    override fun onResume() {
        super.onResume()
        loadVehicles()
    }

}