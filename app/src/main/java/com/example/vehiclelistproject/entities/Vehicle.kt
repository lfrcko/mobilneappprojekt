package com.example.vehiclelistproject.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = User::class,
        childColumns = ["userId"],
        parentColumns = ["id"]
    )]
)
data class Vehicle (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val vehicleName: String,
    val kilometers: Int,
    val userId: Int
)