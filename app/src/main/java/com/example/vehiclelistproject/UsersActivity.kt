package com.example.vehiclelistproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import com.example.vehiclelistproject.databinding.ActivityUsersBinding
import com.example.vehiclelistproject.entities.User
import kotlinx.coroutines.launch

class UsersActivity : MainActivity() {
    private lateinit var binding: ActivityUsersBinding
    private lateinit var usersList: ArrayList<User>
    private lateinit var usersNameList: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUsersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.floatButton.setOnClickListener {
            val intent = Intent(this, UserFormActivity::class.java)
            startActivity(intent)
        }
        binding.lvUsers.setOnItemClickListener { _, _, index, _ ->
            val users = usersList[index]
            val intent = Intent(this, ReadUserActivity::class.java)
            intent.putExtra("userId", users.id)
            startActivity(intent)
        }
    }

    private fun loadTeams() {
        usersList = ArrayList()
        usersNameList = ArrayList()
        lifecycleScope.launch {
            for (user in db.userDao().getUsers()) {
                usersList.add(user)
                usersNameList.add(user.userName)
            }

            val adapter: ArrayAdapter<String?> = ArrayAdapter<String?>(
                this@UsersActivity,
                R.layout.list_items,
                usersNameList as List<String?>
            )

            binding.lvUsers.adapter = adapter
        }
    }

    override fun onResume() {
        super.onResume()
        loadTeams()
    }
}