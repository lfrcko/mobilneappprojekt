package com.example.vehiclelistproject.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.vehiclelistproject.entities.User
import com.example.vehiclelistproject.entities.Vehicle

@Database(
    entities = [
        User::class,
        Vehicle::class
    ],
    version = 1
)
abstract class VehicleDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun vehicleDao(): VehicleDao
}