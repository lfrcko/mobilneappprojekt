package com.example.vehiclelistproject.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.vehiclelistproject.entities.Vehicle


@Dao
interface VehicleDao {
    @Query("SELECT * FROM vehicle")
    suspend fun getVehicles(): List<Vehicle>

    @Query("SELECT * from vehicle WHERE id = :vehicleId")
    suspend fun getVehicleById(vehicleId: Int): Vehicle

    @Insert
    suspend fun insertVehicle(vehicle: Vehicle)

    @Update
    suspend fun updateVehicle(vehicle: Vehicle)

    @Query("DELETE FROM vehicle WHERE id = :vehicleId")
    suspend fun deleteVehicle(vehicleId: Int)
}