package com.example.vehiclelistproject.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.vehiclelistproject.entities.User
import com.example.vehiclelistproject.entities.Vehicle

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    suspend fun getUsers(): List<User>

    @Query("SELECT * from user WHERE id = :userId")
    suspend fun getUserById(userId: Int): User

    @Query("SELECT * FROM vehicle WHERE userID = :userId")
    suspend fun getVehiclesFromUser(userId: Int): List<Vehicle>

    @Insert
    suspend fun insertUser(user: User)

    @Update
    suspend fun updateUser(user: User)

    @Query("DELETE FROM user WHERE id = :userId")
    suspend fun deleteUser(userId: Int)
}