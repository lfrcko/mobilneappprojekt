package com.example.vehiclelistproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import com.example.vehiclelistproject.databinding.ActivityReadUserBinding
import com.example.vehiclelistproject.entities.User
import kotlinx.coroutines.launch

class ReadUserActivity : MainActivity() {
    private lateinit var binding: ActivityReadUserBinding
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityReadUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getUser(this)
    }

    private fun getUser(activity: ReadUserActivity) {
        val userId = intent.getIntExtra("userId", 0)
        if (userId != 0) {
            lifecycleScope.launch {
                user = db.userDao().getUserById(userId)
                binding.datausername.text = user.userName
                binding.datapassword.text = user.password

                binding.editUser.setOnClickListener {
                    val intent = Intent(activity, UserFormActivity::class.java)
                    intent.putExtra("userId", user.id)
                    startActivity(intent)
                }
                binding.deleteUser.setOnClickListener {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle(R.string.remove_user)
                    builder.setMessage(R.string.remove_user_warning)
                    builder.setPositiveButton(R.string.yes) { _, _ ->
                        lifecycleScope.launch {
                            val vehicles = db.userDao().getVehiclesFromUser(userId)
                            for (vehicle in vehicles) {
                                db.vehicleDao().deleteVehicle(vehicle.id)
                            }
                            db.userDao().deleteUser(userId)
                        }
                        finish()
                    }
                    builder.setNegativeButton(R.string.no) { dialog, _ ->
                        dialog.dismiss()
                    }
                    builder.show()
                }
            }
        }
    }

}