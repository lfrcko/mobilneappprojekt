package com.example.vehiclelistproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import com.example.vehiclelistproject.databinding.ActivityVehicleFormBinding
import com.example.vehiclelistproject.entities.Vehicle
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class VehicleFormActivity : MainActivity() {
    private lateinit var binding: ActivityVehicleFormBinding
    private lateinit var vehicle: Vehicle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_form)

        binding = ActivityVehicleFormBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val userMap = mutableMapOf(0 to getString(R.string.choose_user))
        var spinnerList: List<String>

        lifecycleScope.launch {
            for (user in db.userDao().getUsers()) {
                userMap[user.id] = user.userName
            }
            spinnerList = userMap.values.toList()
            val adapter: ArrayAdapter<String?> = ArrayAdapter<String?>(
                this@VehicleFormActivity,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerList as List<String?>
            )
            binding.userId.adapter = adapter

            val vehicleId = intent.getIntExtra("vehicleId", 0)
            if (vehicleId != 0) {
                vehicle = db.vehicleDao().getVehicleById(vehicleId)
                binding.vehicleName.setText(vehicle.vehicleName)
                binding.kilometers.setText(vehicle.kilometers.toString())
                binding.userId.setSelection(spinnerList.indexOf(userMap[vehicle.userId]))
            }

            binding.saveVehicle.setOnClickListener  { view ->
                val vehicleName = binding.vehicleName.text.toString()
                val kilometers = binding.kilometers.text.toString().toIntOrNull()
                val formUserId = binding.userId.selectedItemId

                if (vehicleName == "" || vehicleName.isEmpty()) {
                    Snackbar.make(view, getString(R.string.vehicle_name_error), Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show()
                }
                else if (kilometers == null) {
                    Snackbar.make(view, getString(R.string.vehicle_kilometers_error), Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show()
                }
                else if (formUserId.toInt() == 0) {
                    Snackbar.make(view, getString(R.string.vehicle_userId_empty_error), Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show()
                }
                else {
                    lifecycleScope.launch {
                        val newVehicle = Vehicle(
                            vehicleId,
                            vehicleName,
                            kilometers,
                            userMap.filterValues { it == spinnerList[formUserId.toInt()] }.keys.first()
                        )
                        if (vehicleId == 0) {
                            db.vehicleDao().insertVehicle(newVehicle)
                        }
                        else {
                            db.vehicleDao().updateVehicle(newVehicle)
                        }
                        finish()
                    }
                }
            }
        }

    }
}