package com.example.vehiclelistproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import com.example.vehiclelistproject.databinding.ActivityUserFormBinding
import com.example.vehiclelistproject.entities.User
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class UserFormActivity : MainActivity() {
    private lateinit var binding: ActivityUserFormBinding
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUserFormBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val userId = intent.getIntExtra("userId", 0)
        if (userId != 0) {
            lifecycleScope.launch {
                user = db.userDao().getUserById(userId)
                binding.userName.setText(user.userName)
                binding.userPassword.setText(user.password)
            }
        }

        binding.saveUser.setOnClickListener  {
            val username = binding.userName.text.toString()
            val password = binding.userPassword.text.toString()

            if (username == "" || username.isEmpty()) {
                Snackbar.make(it, getString(R.string.username_error), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show()
            }
            else if (password == "" || password.isEmpty()) {
                Snackbar.make(it, getString(R.string.password_error), Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show()
            }
            else {
                lifecycleScope.launch {
                    val newUser = User(userId, username, password)
                    if (userId == 0) {
                        db.userDao().insertUser(newUser)
                    }
                    else {
                        db.userDao().updateUser(newUser)
                    }
                    finish()
                }
            }
        }
    }
}