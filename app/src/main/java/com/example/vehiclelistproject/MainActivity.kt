package com.example.vehiclelistproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import com.example.vehiclelistproject.database.VehicleDatabase
import com.example.vehiclelistproject.databinding.ActivityMainBinding

open class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var databaseExists = false
    lateinit var db: VehicleDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (databaseExists == false) {
            initiliazeDatabase()
        }

        binding.btnUsers.setOnClickListener {
            val intent = Intent(this, UsersActivity::class.java)
            startActivity(intent)
        }
        binding.btnVehicles.setOnClickListener {
            val intent = Intent(this, VehiclesActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initiliazeDatabase() {
        db = Room.databaseBuilder(
            applicationContext,
            VehicleDatabase::class.java, "f1-manager-db"
        ).build()
        databaseExists = true
    }
}